# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/Xint0/soap-service-client/compare/0.1.1...head)

## [0.1.1](https://gitlab.com/Xint0/soap-service-client/-/compare/0.1.0...0.1.1) - 2022-02-28

### Removed

- Remove `SoapServiceClientFactory` implementation and tests.

## [0.1.0](https://gitlab.com/Xint0/soap-service-client/tags/0.1.0) - 2022-02-25

### Added

- Initial release with `SoapServiceClient`.