<?php

/**
 * SOAP service client
 *
 * @copyright Copyright 2022 Rogelio Jacinto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package soap-service-client
 * @license https://gitlab.com/xint0/soap-service-client/-/blob/main/LICENSE Apache License 2.0
 */

declare(strict_types=1);

namespace Xint0\Soap\Tests;

use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use SoapFault;
use Xint0\Soap\SoapServiceClient;

/**
 * @covers \Xint0\Soap\SoapServiceClient
 */
class SoapServiceClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function test_can_create_instance(): void
    {
        $sut = new SoapServiceClient(null, ['location'=>'https://example.com', 'uri' => 'https://example.com']);
        $this->assertNotNull($sut);
    }

    public function test_logs_expected_message_on_instance_creation(): void
    {
        $wsdl = 'https://dev.advans.mx/cfdi-cancelacion/soap?wsdl';
        $soapClientOptions = [
            'keep_alive' => false,
            'trace' => true,
        ];
        $mockLogger = \Mockery::mock(LoggerInterface::class);
        $mockLogger->shouldReceive('debug')
            ->once()
            ->withArgs(function (string $message, array $context) use ($wsdl, $soapClientOptions) {
                return $message === SoapServiceClient::class . '::__construct - created SoapClient instance.' &&
                    array_key_exists('wsdl', $context) && $context['wsdl'] === $wsdl &&
                    array_key_exists('options', $context) && is_array($context['options']) &&
                    $context['options'] === $soapClientOptions;
            });
        $mockLogger->makePartial();

        $sut = new SoapServiceClient($wsdl, $soapClientOptions + [ 'logger' => $mockLogger ]);
        $this->assertNotNull($sut);
    }

    public function test_logs_expected_message_on_instance_creation_failure(): void
    {
        $wsdl = 'file://./does-not-exist.wsdl';
        $soapClientOptions = [
            'keep_alive' => false,
            'trace' => true,
        ];
        $mockLogger = \Mockery::mock(LoggerInterface::class);
        $mockLogger->shouldReceive('error')
            ->once()
            ->withArgs(function (string $message, array $context) use ($wsdl, $soapClientOptions) {
                return $message === SoapServiceClient::class . '::__construct - could not create SoapClient instance.' &&
                    array_key_exists('wsdl', $context) && $context['wsdl'] === $wsdl &&
                    array_key_exists('options', $context) && is_array($context['options']) &&
                    $context['options'] === $soapClientOptions &&
                    array_key_exists('exception', $context) && $context['exception'] instanceof SoapFault;
            });
        $mockLogger->makePartial();

        $this->expectException(SoapFault::class);
        new SoapServiceClient($wsdl, $soapClientOptions + [ 'logger' => $mockLogger ]);
    }

    public function test_logs_expected_message_on_method_call_failure(): void
    {
        $wsdl = 'https://dev.advans.mx/cfdi-cancelacion/soap?wsdl';
        $streamContext = stream_context_create([
            'http' => [
                'header' => 'Authorization: wrong key to force SoapFault',
            ],
        ]);
        $soapClientOptions = [
            'keep_alive' => false,
            'trace' => true,
            'stream_context' => $streamContext,
        ];
        $mockLogger = \Mockery::mock(LoggerInterface::class);
        $mockLogger->shouldReceive('debug')
            ->once()
            ->withArgs(function (string $message, array $context) use ($wsdl, $soapClientOptions) {
                return $message === SoapServiceClient::class . '::__construct - created SoapClient instance.' &&
                    array_key_exists('wsdl', $context) && $context['wsdl'] === $wsdl &&
                    array_key_exists('options', $context) && is_array($context['options']) &&
                    $context['options'] === $soapClientOptions;
            });
        $mockLogger->shouldReceive('error')
            ->once()
            ->withArgs(function (string $message, array $context) {
                return $message === SoapServiceClient::class . "::__call - method 'Cancelar' failed." &&
                    array_key_exists('method', $context) &&
                    array_key_exists('params', $context) &&
                    array_key_exists('request', $context) &&
                    array_key_exists('requestHeaders', $context) &&
                    array_key_exists('exception', $context);
            });
        $mockLogger->makePartial();

        $sut = new SoapServiceClient($wsdl, $soapClientOptions + [ 'logger' => $mockLogger ]);

        $this->expectException(SoapFault::class);

        $sut->Cancelar([
            'PrivateKeyPem' => '',
            'PublicKeyPem' => '',
            'Uuid' => '',
            'RfcReceptor' => '',
            'Total' => '',
            'Motivo' => '02',
            'FolioSustitucion' => '',
        ]);
    }

    public function test_logs_expected_message_on_method_call_success(): void
    {
        $wsdl = 'https://dev.advans.mx/cfdi-cancelacion/soap?wsdl';
        $streamContext = stream_context_create([
            'http' => [
                'header' => 'Authorization: 8d3ba1ff600a9ac056d923429ef97886',
            ],
        ]);
        $soapClientOptions = [
            'keep_alive' => false,
            'trace' => true,
            'stream_context' => $streamContext,
        ];
        $mockLogger = \Mockery::mock(LoggerInterface::class);
        $mockLogger->shouldReceive('debug')
            ->once()
            ->withArgs(function (string $message, array $context) use ($wsdl, $soapClientOptions) {
                return $message === SoapServiceClient::class . '::__construct - created SoapClient instance.' &&
                    array_key_exists('wsdl', $context) && $context['wsdl'] === $wsdl &&
                    array_key_exists('options', $context) && is_array($context['options']) &&
                    $context['options'] == $soapClientOptions;
            });
        $mockLogger->shouldReceive('debug')
            ->once()
            ->withArgs(function (string $message, array $context) {
                return $message === SoapServiceClient::class . "::__call - method 'Cancelar' success." &&
                    array_key_exists('method', $context) &&
                    array_key_exists('params', $context) &&
                    array_key_exists('request', $context) &&
                    array_key_exists('requestHeaders', $context) &&
                    array_key_exists('response', $context) &&
                    array_key_exists('responseHeaders', $context) &&
                    array_key_exists('result', $context);
            });
        $mockLogger->makePartial();

        $sut = new SoapServiceClient($wsdl, $soapClientOptions + [ 'logger' => $mockLogger ]);

        $sut->Cancelar([
            'PrivateKeyPem' => '',
            'PublicKeyPem' => '',
            'Uuid' => '',
            'RfcReceptor' => '',
            'Total' => '',
            'Motivo' => '02',
            'FolioSustitucion' => '',
        ]);
    }

    public function test_returns_expected_value_when_action_call_succeeds(): void
    {
        $wsdl = 'https://dev.advans.mx/cfdi-cancelacion/soap?wsdl';
        $streamContext = stream_context_create([
            'http' => [
                'header' => 'Authorization: 8d3ba1ff600a9ac056d923429ef97886',
            ],
        ]);
        $soapClientOptions = [
            'keep_alive' => false,
            'trace' => true,
            'stream_context' => $streamContext,
        ];
        $expectedResult = (object)[
            'Code' => '997',
            'Message' => 'Parámetros inválidos',
            'Id' => null,
            'Detail' => 'UUID inválido',
        ];
        $sut = new SoapServiceClient($wsdl, $soapClientOptions);
        $actualResult = $sut->Cancelar([
            'PrivateKeyPem' => '',
            'PublicKeyPem' => '',
            'Uuid' => '',
            'RfcReceptor' => '',
            'Total' => '',
            'Motivo' => '02',
            'FolioSustitucion' => '',
        ]);
        $this->assertEquals($expectedResult, $actualResult);
    }
}
