# SOAP service client

`xint0/soap-service-client` is a library that contains `SoapServiceClient` class that extends the base `SoapClient` class.

## Installation

Use [composer](https://getcomposer.org) to install `xint0/soap-service-client`.

```bash
composer require xint0/soap-service-client
```

## Usage

```php
<?php

use Xint0\Soap\SoapServiceClient;
use Psr\Log\LoggerInterface;

/** @var LoggerInterface A logger. */
$logger;

$wsdl = 'https://example.com/route_to_wsdl';
$options = [
    'logger' => $logger, // You can pass a LoggerInterface to log debug messages on SOAP calls and errors.
];

$soapClient = new SoapServiceClient($wsdl, $options);

$actionParameters = [
    'param' => 'value',
];
$result = $soapClient->SoapAction($actionParameters);
```

## Contributing

Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)