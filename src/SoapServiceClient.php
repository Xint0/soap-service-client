<?php

/**
 * SOAP service client
 *
 * @copyright Copyright 2022 Rogelio Jacinto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package soap-service-client
 * @license https://gitlab.com/xint0/soap-service-client/-/blob/main/LICENSE Apache License 2.0
 */

declare(strict_types=1);

namespace Xint0\Soap;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SoapClient;
use SoapFault;

class SoapServiceClient extends SoapClient
{
    use LoggerAwareTrait;

    public function __construct($wsdl, array $options = null)
    {
        if (array_key_exists('logger', $options)) {
            $logger = $options['logger'] instanceof LoggerInterface ? $options['logger'] : new NullLogger();
            unset($options['logger']);
        }
        $this->logger = $logger ?? new NullLogger();

        try {
            parent::__construct($wsdl, $options);
        } catch (SoapFault $exception) {
            $this->logger->error(
                __METHOD__ . ' - could not create SoapClient instance.',
                compact([ 'wsdl', 'options', 'exception' ])
            );
            throw $exception;
        }

        $this->logger->debug(__METHOD__ . ' - created SoapClient instance.', compact(['wsdl', 'options']));
    }

    /**
     * @throws SoapFault
     */
    public function __call($name, $args)
    {
        try {
            $result = parent::__call($name, $args);
        } catch (SoapFault $exception) {
            $this->logger->error(
                __METHOD__ . " - method '$name' failed.",
                [
                    'method' => $name,
                    'params' => $args,
                    'request' => $this->__getLastRequest(),
                    'requestHeaders' => $this->__getLastRequestHeaders(),
                    'exception' => $exception,
                ]
            );

            throw $exception;
        }

        $this->logger->debug(
            __METHOD__ . " - method '$name' success.",
            [
                'method' => $name,
                'params' => $args,
                'request' => $this->__getLastRequest(),
                'requestHeaders' => $this->__getLastRequestHeaders(),
                'response' => $this->__getLastResponse(),
                'responseHeaders' => $this->__getLastResponseHeaders(),
                'result' => $result,
            ]
        );

        return $result;
    }
}